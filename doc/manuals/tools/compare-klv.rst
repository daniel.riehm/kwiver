===========
compare-klv
===========

This program prints differences found between the KLV in two files (video or JSON).

kwiver compare-klv [options] lhs-file rhs-file
--------------------------------------------

  lhs-file: Left-hand-side video or JSON file for comparison.

  rhs-file: Right-hand-side video or JSON file for comparison.


**Options are:**

  ``-h, --help``                 Display applet usage.

  ``-c, --config filename``      Provide configuration file.
