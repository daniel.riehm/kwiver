======================
init-cameras-landmarks
======================

kwiver init-cameras-landmarks       [options]
--------------------------------------

**Options are:**

  ``-h, --help``
    Display applet usage information.

  ``-c, --config arg``
    Configuration file for tool

  ``-o, --output-config arg``
    Output a configuration. This may be seeded with a configuration file from -c/--config.

  ``-v, --video arg``
    Input video

  ``-t, --tracks arg``
    Input tracks

  ``-k, --camera arg``
    Output directory for cameras

  ``-l, --landmarks arg``
    Output landmarks file

  ``-g, --geo-origin arg``
    Output geographic origin file
