Ceres
=====

..  _ceres_bundle_adjust:
Bundle Adjust Algorithm
-----------------------

..  doxygenclass:: kwiver::arrows::ceres::bundle_adjust
    :project: kwiver
    :members:


..  _ceres_optimize_cameras:

Optimize Cameras Algorithm
--------------------------

..  doxygenclass:: kwiver::arrows::ceres::optimize_cameras
    :project: kwiver
    :members:
