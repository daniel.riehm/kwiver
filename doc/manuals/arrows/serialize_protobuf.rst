Serialize Protobuf
==================

.. _protobuf_activity_type:

Serialize Protobuf Activity Type Algorithm
-------------------------------------------

..  doxygenclass:: kwiver::arrows::serialize::protobuf::activity_type
    :project: kwiver
    :members:


.. _protobuf_activity:

Serialize Protobuf Activity Algorithm
---------------------------------------

..  doxygenclass:: kwiver::arrows::serialize::protobuf::activity
    :project: kwiver
    :members:


.. _protobuf_bounding_box:

Serialize Protobuf Bounding Box Algorithm
------------------------------------------

..  doxygenclass:: kwiver::arrows::serialize::protobuf::bounding_box
    :project: kwiver
    :members:


.. _protobuf_detected_object_set:

Serialize Protobuf Detected Object Set Algorithm
-------------------------------------------------

..  doxygenclass:: kwiver::arrows::serialize::protobuf::detected_object_set
    :project: kwiver
    :members:


.. _protobuf_detected_object_type:

Serialize Protobuf Detected Object Type Algorithm
--------------------------------------------------

..  doxygenclass:: kwiver::arrows::serialize::protobuf::detected_object_type
    :project: kwiver
    :members:


.. _protobuf_detected_object:

Serialize Protobuf Detected Object Algorithm
---------------------------------------------

..  doxygenclass:: kwiver::arrows::serialize::protobuf::detected_object
    :project: kwiver
    :members:


.. _protobuf_geo_polygon:

Serialize Protobuf Geo Polygon Algorithm
-----------------------------------------

..  doxygenclass:: kwiver::arrows::serialize::protobuf::geo_polygon
    :project: kwiver
    :members:


.. _protobuf_image:

Serialize Protobuf Image Algorithm
-----------------------------------

..  doxygenclass:: kwiver::arrows::serialize::protobuf::image
    :project: kwiver
    :members:


.. _protobuf_metadata_map_io:

Serialize Protobuf Metadata Map Input/Output Algorithm
-------------------------------------------------------

..  doxygenclass:: kwiver::arrows::serialize::protobuf::metadata_map_io
    :project: kwiver
    :members:


.. _protobuf_metadata:

Serialize Protobuf Metadata Algorithm
--------------------------------------

..  doxygenclass:: kwiver::arrows::serialize::protobuf::metadata
    :project: kwiver
    :members:


.. _protobuf_object_track_set:

Serialize Protobuf Object Track Set Algorithm
---------------------------------------------

..  doxygenclass:: kwiver::arrows::serialize::protobuf::object_track_set
    :project: kwiver
    :members:


.. _protobuf_object_track_state:

Serialize Protobuf Object Track State Algorithm
------------------------------------------------

..  doxygenclass:: kwiver::arrows::serialize::protobuf::object_track_state
    :project: kwiver
    :members:


.. _protobuf_string:

Serialize Protobuf String Algorithm
------------------------------------

..  doxygenclass:: kwiver::arrows::serialize::protobuf::string
    :project: kwiver
    :members:


.. _protobuf_timestamp:

Serialize Protobuf Timestamp Algorithm
---------------------------------------

..  doxygenclass:: kwiver::arrows::serialize::protobuf::timestamp
    :project: kwiver
    :members:


.. _protobuf_track_set:

Serialize Protobuf Track Set Algorithm
----------------------------------------

..  doxygenclass:: kwiver::arrows::serialize::protobuf::track_set
    :project: kwiver
    :members:


.. _protobuf_track_state:

Serialize Protobuf Track State Algorithm
------------------------------------------

..  doxygenclass:: kwiver::arrows::serialize::protobuf::track_state
    :project: kwiver
    :members:


.. _protobuf_track:

Serialize Protobuf Track Algorithm
------------------------------------

..  doxygenclass:: kwiver::arrows::serialize::protobuf::track
    :project: kwiver
    :members:
