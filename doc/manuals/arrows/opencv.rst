OpenCV
======

This arrow is a collection of vital algorithms implemented with the OpenCV API

This arrow can be built by enabling the KWIVER_ENABLE_OPENCV CMake flag

This arrow contains the following functionality:

.. _ocv_analyze_tracks:

Analyze Tracks Algorithm
------------------------

..  doxygenclass:: kwiver::arrows::ocv::analyze_tracks
    :project: kwiver
    :members:

.. _ocv_detect_features:

Detect Features Algorithm
-------------------------------

..  doxygenclass:: kwiver::arrows::ocv::detect_features
    :project: kwiver
    :members:

.. _ocv_detect_features_AGAST:

Detect Features AGAST Algorithm
-------------------------------

..  doxygenclass:: kwiver::arrows::ocv::detect_features_AGAST
    :project: kwiver
    :members:

.. _ocv_detect_features_FAST:

Detect Features FAST Algorithm
------------------------------

..  doxygenclass:: kwiver::arrows::ocv::detect_features_FAST
    :project: kwiver
    :members:

.. _ocv_detect_features_GFTT:

Detect Features GFTT Algorithm
------------------------------

..  doxygenclass:: kwiver::arrows::ocv::detect_features_GFTT
    :project: kwiver
    :members:

.. _ocv_detect_features_MSD:

Detect Features MSD Algorithm
-----------------------------

..  doxygenclass:: kwiver::arrows::ocv::detect_features_MSD
    :project: kwiver
    :members:

.. _ocv_detect_features_MSER:

Detect Features MSER Algorithm
------------------------------

..  doxygenclass:: kwiver::arrows::ocv::detect_features_MSER
    :project: kwiver
    :members:

.. _ocv_detect_features_simple_blob:

Detect Features Simple BLOB Algorithm
-------------------------------------

..  doxygenclass:: kwiver::arrows::ocv::detect_features_simple_blob
    :project: kwiver
    :members:

.. _ocv_detect_features_STAR:

Detect Features STAR Algorithm
------------------------------

..  doxygenclass:: kwiver::arrows::ocv::detect_features_STAR
    :project: kwiver
    :members:

.. _ocv_detect_heat_map:

Detect Heat Map Algorithm
-------------------------

..  doxygenclass:: kwiver::arrows::ocv::detect_heat_map
    :project: kwiver
    :members:

.. _ocv_detect_motion_3frame_differencing:

Detect Motion 3-Frame Differencing Algorithm
----------------------------------------------

..  doxygenclass:: kwiver::arrows::ocv::detect_motion_3frame_differencing
    :project: kwiver
    :members:

.. _ocv_detect_motion_mog2:

Detect Motion MOG2 Algorithm
-----------------------------

..  doxygenclass:: kwiver::arrows::ocv::detect_motion_mog2
    :project: kwiver
    :members:

.. _ocv_draw_detected_object_set:

Draw Detected Object Set Algorithm
----------------------------------

..  doxygenclass:: kwiver::arrows::ocv::draw_detected_object_set
    :project: kwiver
    :members:

.. _ocv_draw_tracks:

Draw Tracks Algorithm
---------------------

..  doxygenclass:: kwiver::arrows::ocv::draw_tracks
    :project: kwiver
    :members:

.. _ocv_estimate_fundamental_matrix:

Estimate Fundamental Matrix Algorithm
-------------------------------------

..  doxygenclass:: kwiver::arrows::ocv::estimate_fundamental_matrix
    :project: kwiver
    :members:

.. _ocv_estimate_homography:

Estimate Homography Algorithm
-----------------------------

..  doxygenclass:: kwiver::arrows::ocv::estimate_homography
    :project: kwiver
    :members:

.. _ocv_estimate_pnp:

Estimate PnP Algorithm
-----------------------

..  doxygenclass:: kwiver::arrows::ocv::estimate_pnp
    :project: kwiver
    :members:

.. _ocv_extract_descriptors:

Extract Descriptors Algorithm
-----------------------------------

..  doxygenclass:: kwiver::arrows::ocv::extract_descriptors_BRIEF
    :project: kwiver
    :members:

.. _ocv_extract_descriptors_BRIEF:

Extract Descriptors BRIEF Algorithm
-----------------------------------

..  doxygenclass:: kwiver::arrows::ocv::extract_descriptors_BRIEF
    :project: kwiver
    :members:

.. _ocv_extract_descriptors_DAISY:

Extract Descriptors DAISY Algorithm
-----------------------------------

..  doxygenclass:: kwiver::arrows::ocv::extract_descriptors_DAISY
    :project: kwiver
    :members:

.. _ocv_extract_descriptors_FREAK:

Extract Descriptors FREAK Algorithm
-----------------------------------

..  doxygenclass:: kwiver::arrows::ocv::extract_descriptors_FREAK
    :project: kwiver
    :members:

.. _ocv_extract_descriptors_LATCH:

Extract Descriptors LATCH Algorithm
-----------------------------------

..  doxygenclass:: kwiver::arrows::ocv::extract_descriptors_LATCH
    :project: kwiver
    :members:

.. _ocv_extract_descriptors_LUCID:

Extract Descriptors LUCID Algorithm
-----------------------------------

..  doxygenclass:: kwiver::arrows::ocv::extract_descriptors_LUCID
    :project: kwiver
    :members:

.. _ocv_detect_features_BRISK:

Detect Features with BRISK Algorithm
-----------------------------------------------------

..  doxygenclass:: kwiver::arrows::ocv::detect_features_BRISK
    :project: kwiver
    :members:

.. _ocv_detect_features_ORB:

Detect Features with ORB Algorithm
-----------------------------------------------------

..  doxygenclass:: kwiver::arrows::ocv::detect_features_ORB
    :project: kwiver
    :members:

.. _ocv_detect_features_SIFT:

Detect Features with SIFT Algorithm
-----------------------------------------------------

..  doxygenclass:: kwiver::arrows::ocv::detect_features_SIFT
    :project: kwiver
    :members:

.. _ocv_detect_features_SURF:

Detect Features with SURF Algorithm
-----------------------------------------------------

..  doxygenclass:: kwiver::arrows::ocv::detect_features_SURF
    :project: kwiver
    :members:

.. _ocv_hough_circle_detector:

Hough Circle Detector Algorithm
-------------------------------

..  doxygenclass:: kwiver::arrows::ocv::hough_circle_detector
    :project: kwiver
    :members:

.. _ocv_image_io:

Image I/O Algorithm
-------------------

..  doxygenclass:: kwiver::arrows::ocv::image_io
    :project: kwiver
    :members:


.. _ocv_inpaint:

Inpaint Algorithm
---------------------

..  doxygenclass:: kwiver::arrows::ocv::inpaint
    :project: kwiver
    :members:


.. _ocv_match_features_bruteforce:

Match Features Bruteforce Algorithm
-----------------------------------

..  doxygenclass:: kwiver::arrows::ocv::match_features_bruteforce
    :project: kwiver
    :members:

.. _ocv_match_features_flannbased:

Match Features Flannbased Algorithm
-----------------------------------

..  doxygenclass:: kwiver::arrows::ocv::match_features_flannbased
    :project: kwiver
    :members:

.. _ocv_merge_images:

Merge Images Algorithm
-----------------------

..  doxygenclass:: kwiver::arrows::ocv::merge_images
    :project: kwiver
    :members:

.. _ocv_refine_detections_write_to_disk:

Refine Detections Write To Disk Algorithm
-----------------------------------------

..  doxygenclass:: kwiver::arrows::ocv::refine_detections_write_to_disk
    :project: kwiver
    :members:

.. _ocv_split_image:

Split Image Algorithm
---------------------

..  doxygenclass:: kwiver::arrows::ocv::split_image
    :project: kwiver
    :members:

.. _ocv_track_features_klt:

Track Features using KLT Algorithm
-----------------------------------

..  doxygenclass:: kwiver::arrows::ocv::track_features_klt
    :project: kwiver
    :members:
