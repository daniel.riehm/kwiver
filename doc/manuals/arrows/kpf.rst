KPF
===

.. _kpf_detected_object_set_input_kpf:

Detected Object Set Input KPF Algorithm
----------------------------------------

..  doxygenclass:: kwiver::arrows::kpf::detected_object_set_input_kpf
    :project: kwiver
    :members:


.. _kpf_detected_object_set_output_kpf:

Detected Object Set Output KPF Algorithm
-----------------------------------------

..  doxygenclass:: kwiver::arrows::kpf::detected_object_set_output_kpf
    :project: kwiver
    :members:
