Super3d
========

.. _super3d_compute_depth:

Compute Depth Algorithm (Super3D)
----------------------------------

..  doxygenclass:: kwiver::arrows::super3d::compute_depth
    :project: kwiver
    :members:
