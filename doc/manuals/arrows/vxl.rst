VXL
======

This arrow is a collection of vital algorithms implemented with the VXL API

This arrow can be built by enabling the KWIVER_ENABLE_VXL CMake flag

This arrow contains the following functionality:

.. _vxl_aligned_edge_detection:

Aligned Edge Detection Algorithm
--------------------------------

..  doxygenclass:: kwiver::arrows::vxl::aligned_edge_detection
    :project: kwiver
    :members:

.. _vxl_average_frames:

Average Frames Algorithm
-------------------------

..  doxygenclass:: kwiver::arrows::vxl::average_frames
    :project: kwiver
    :members:

.. _vxl_bundle_adjust:

Bundle Adjust Algorithm
------------------------

..  doxygenclass:: kwiver::arrows::vxl::bundle_adjust
    :project: kwiver
    :members:

.. _vxl_close_loops_homography_guided:

Close Loops Homography Guided Algorithm
-----------------------------------------

..  doxygenclass:: kwiver::arrows::vxl::close_loops_homography_guided
    :project: kwiver
    :members:

.. _vxl_color_commonality_filter:

Color Commonality Filter Algorithm
------------------------------------

..  doxygenclass:: kwiver::arrows::vxl::color_commonality_filter
    :project: kwiver
    :members:

.. _vxl_convert_image:

Convert Image Algorithm
-------------------------

..  doxygenclass:: kwiver::arrows::vxl::convert_image
    :project: kwiver
    :members:

.. _vxl_estimate_canonical_transform:

Estimate Canonical Transform Algorithm
----------------------------------------

..  doxygenclass:: kwiver::arrows::vxl::estimate_canonical_transform
    :project: kwiver
    :members:

.. _vxl_estimate_essential_matrix:

Estimate Essential Matrix Algorithm
-------------------------------------

..  doxygenclass:: kwiver::arrows::vxl::estimate_essential_matrix
    :project: kwiver
    :members:

.. _vxl_estimate_fundamental_matrix:

Estimate Fundamental Matrix Algorithm
----------------------------------------

..  doxygenclass:: kwiver::arrows::vxl::estimate_fundamental_matrix
    :project: kwiver
    :members:

.. _vxl_estimate_homography:

Estimate Homography Algorithm
-------------------------------

..  doxygenclass:: kwiver::arrows::vxl::estimate_homography
    :project: kwiver
    :members:

.. _vxl_estimate_similarity_transform:

Estimate Similarity Transform Algorithm
-----------------------------------------

..  doxygenclass:: kwiver::arrows::vxl::estimate_similarity_transform
    :project: kwiver
    :members:

.. _vxl_hashed_image_classifier_filter:

Hashed Image Classifier Filter Algorithm
-------------------------------------------

..  doxygenclass:: kwiver::arrows::vxl::hashed_image_classifier_filter
    :project: kwiver
    :members:

.. _vxl_high_pass_filter:

High Pass Filter Algorithm
---------------------------

..  doxygenclass:: kwiver::arrows::vxl::high_pass_filter
    :project: kwiver
    :members:

.. _vxl_image_io:

Image I/O Algorithm
---------------------

..  doxygenclass:: kwiver::arrows::vxl::image_io
    :project: kwiver
    :members:

.. _vxl_kd_tree:

KD Tree Algorithm
------------------

..  doxygenclass:: kwiver::arrows::vxl::kd_tree
    :project: kwiver
    :members:

.. _vxl_match_features_constrained:

Match Features Constrained Algorithm
--------------------------------------

..  doxygenclass:: kwiver::arrows::vxl::match_features_constrained
    :project: kwiver
    :members:

.. _vxl_morphology:

Morphology Algorithm
----------------------

..  doxygenclass:: kwiver::arrows::vxl::morphology
    :project: kwiver
    :members:

.. _vxl_optimize_cameras:

Optimize Cameras Algorithm
----------------------------

..  doxygenclass:: kwiver::arrows::vxl::optimize_cameras
    :project: kwiver
    :members:

.. _vxl_pixel_feature_extractor:

Pixel Feature Extractor Algorithm
------------------------------------

..  doxygenclass:: kwiver::arrows::vxl::pixel_feature_extractor
    :project: kwiver
    :members:

.. _vxl_split_image:

Split Image Algorithm
-----------------------

..  doxygenclass:: kwiver::arrows::vxl::split_image
    :project: kwiver
    :members:

.. _vxl_threshold:

Threshold Algorithm
---------------------

..  doxygenclass:: kwiver::arrows::vxl::threshold
    :project: kwiver
    :members:

.. _vxl_triangulate_landmarks:

Triangulate Landmarks Algorithm
---------------------------------

..  doxygenclass:: kwiver::arrows::vxl::triangulate_landmarks
    :project: kwiver
    :members:
