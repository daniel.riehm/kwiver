Core
====

.. _core_associate_detections_to_tracks_threshold:

Associate Detections to Tracks Threshold Algorithm
--------------------------------------------------

..  doxygenclass:: kwiver::arrows::core::associate_detections_to_tracks_threshold
    :project: kwiver
    :members:

.. _core_class_probability_filter:

Class Probablity Filter Algorithm
---------------------------------

..  doxygenclass:: kwiver::arrows::core::class_probability_filter
    :project: kwiver
    :members:

.. _core_close_loops_appearance_indexed:

Close Loops Appearance Indexed Algorithm
----------------------------------------

..  doxygenclass:: kwiver::arrows::core::close_loops_appearance_indexed
    :project: kwiver
    :members:

.. _core_close_loops_bad_frames_only:

Close Loops Bad Frames Only Algorithm
-------------------------------------

..  doxygenclass:: kwiver::arrows::core::close_loops_bad_frames_only
    :project: kwiver
    :members:

.. _core_close_loops_exhaustive:

Close Loops Exhaustive Algorithm
--------------------------------

..  doxygenclass:: kwiver::arrows::core::close_loops_exhaustive
    :project: kwiver
    :members:

.. _core_close_loops_keyframe:

Close Loops Keyframe Algorithm
------------------------------

..  doxygenclass:: kwiver::arrows::core::close_loops_keyframe
    :project: kwiver
    :members:

.. _core_close_loops_multi_method:

Close Loops Multi Method Algorithm
----------------------------------

..  doxygenclass:: kwiver::arrows::core::close_loops_multi_method
    :project: kwiver
    :members:

.. _core_compute_association_matrix_from_features:

Compute Association Matrix from Features Algorithm
--------------------------------------------------

..  doxygenclass:: kwiver::arrows::core::compute_association_matrix_from_features
    :project: kwiver
    :members:

.. _core_compute_ref_homography_core:

Compute Ref Homography Core Algorithm
-------------------------------------

..  doxygenclass:: kwiver::arrows::core::compute_ref_homography_core
    :project: kwiver
    :members:

.. _core_convert_image_bypass:

Convert Image Bypass Algorithm
------------------------------

..  doxygenclass:: kwiver::arrows::core::convert_image_bypass
    :project: kwiver
    :members:

.. _core_create_detection_grid:

Create Detection Grid Algorithm
------------------------------

..  doxygenclass:: kwiver::arrows::core::create_detection_grid
    :project: kwiver
    :members:

.. _core_derive_metadata:

Derive Metada Algorithm
------------------------------

..  doxygenclass:: kwiver::arrows::core::derive_metadata
    :project: kwiver
    :members:

.. _core_detect_features_filtered:

Detected Features Filtered Algorithm
---------------------------------------

..  doxygenclass:: kwiver::arrows::core::detect_features_filtered
    :project: kwiver
    :members:

.. _core_detected_object_set_input_csv:

Detected Object Set Input csv Algorithm
---------------------------------------

..  doxygenclass:: kwiver::arrows::core::detected_object_set_input_csv
    :project: kwiver
    :members:

.. _core_detected_object_set_input_kw18:

Detected Object Set Input kw18 Algorithm
----------------------------------------

..  doxygenclass:: kwiver::arrows::core::detected_object_set_input_kw18
    :project: kwiver
    :members:

.. _core_detected_object_set_output_csv:

Detected Object Set Output csv Algorithm
----------------------------------------

..  doxygenclass:: kwiver::arrows::core::detected_object_set_output_csv
    :project: kwiver
    :members:

.. _core_detected_object_set_output_kw18:

Detected Object Set Output kw18 Algorithm
-----------------------------------------

..  doxygenclass:: kwiver::arrows::core::detected_object_set_output_kw18
    :project: kwiver
    :members:

.. _core_dynamic_config_none:

Dynamic Config None Algorithm
-----------------------------

..  doxygenclass:: kwiver::arrows::core::dynamic_config_none
    :project: kwiver
    :members:

.. _core_estimate_canonical_transform:

Estimate Canonical Transform Algorithm
--------------------------------------

..  doxygenclass:: kwiver::arrows::core::estimate_canonical_transform
    :project: kwiver
    :members:

.. _core_example_detector:

Example Detector Algorithm
--------------------------------------

..  doxygenclass:: kwiver::arrows::core::example_detector
    :project: kwiver
    :members:

.. _core_feature_descriptor_io:

Feature Descriptor I/O Algorithm
--------------------------------

..  doxygenclass:: kwiver::arrows::core::feature_descriptor_io
    :project: kwiver
    :members:

.. _core_filter_features_magnitude:

Filter Features Magnitude Algorithm
-----------------------------------

..  doxygenclass:: kwiver::arrows::core::filter_features_magnitude
    :project: kwiver
    :members:

.. _core_filter_features_nonmax:

Filter Features Nonmax Algorithm
------------------------------

..  doxygenclass:: kwiver::arrows::core::filter_features_nonmax
    :project: kwiver
    :members:

.. _core_filter_features_scale:

Filter Features Scale Algorithm
------------------------------

..  doxygenclass:: kwiver::arrows::core::filter_features_scale
    :project: kwiver
    :members:

.. _core_filter_tracks:

Filter Tracks Algorithm
------------------------

..  doxygenclass:: kwiver::arrows::core::filter_tracks
    :project: kwiver
    :members:

.. _core_handle_descriptor_request_core:

Handle Descriptor Request Core Algorithm
----------------------------------------

..  doxygenclass:: kwiver::arrows::core::handle_descriptor_request_core
    :project: kwiver
    :members:

.. _core_initialize_object_tracks_threshold:

Initialize Object Tracks Threshold Algorithm
--------------------------------------------

..  doxygenclass:: kwiver::arrows::core::initialize_object_tracks_threshold
    :project: kwiver
    :members:

.. _core_interpolate_track_spline:

Interpolate Track Spline Algorithm
--------------------------------------------

..  doxygenclass:: kwiver::arrows::core::interpolate_track_spline
    :project: kwiver
    :members:

.. _core_keyframe_selector_basic:

Keyframe Selector Basic Algorithm
--------------------------------------------

..  doxygenclass:: kwiver::arrows::core::keyframe_selector_basic
    :project: kwiver
    :members:

.. _core_match_features_fundamental_matrix:

Match Features Fundamental Matrix Algorithm
-------------------------------------------

..  doxygenclass:: kwiver::arrows::core::match_features_fundamental_matrix
    :project: kwiver
    :members:

.. _core_match_features_homography:

Match Features Homography Algorithm
-----------------------------------

..  doxygenclass:: kwiver::arrows::core::match_features_homography
    :project: kwiver
    :members:

.. _core_merge_metadata_streams:

Merge Metadata Streams Algorithm
-----------------------------------

..  doxygenclass:: kwiver::arrows::core::merge_metadata_streams
    :project: kwiver
    :members:

.. _core_metadata_map_io_csv:

Metadata Map I/O csv Algorithm
-----------------------------------

..  doxygenclass:: kwiver::arrows::core::metadata_map_io_csv
    :project: kwiver
    :members:

.. _core_read_object_track_set_kw18:

Read Object Track Set kw18 Algorithm
-----------------------------------

..  doxygenclass:: kwiver::arrows::core::read_object_track_set_kw18
    :project: kwiver
    :members:

.. _core_read_track_descriptor_set_csv:

Read Track Descriptor Set csv Algorithm
-----------------------------------

..  doxygenclass:: kwiver::arrows::core::read_track_descriptor_set_csv
    :project: kwiver
    :members:

.. _core_track_features_augment_keyframes:

Track Features Augment Keyframes Algorithm
-------------------------------------------

..  doxygenclass:: kwiver::arrows::core::track_features_augment_keyframes
    :project: kwiver
    :members:

.. _core_track_features_core:

Track Features Core Algorithm
-----------------------------

..  doxygenclass:: kwiver::arrows::core::track_features_core
    :project: kwiver
    :members:

.. _core_transfer_bbox_with_depth_map:

Transfer Bounding Box with Depth Map Algorithm
-------------------------------------------

..  doxygenclass:: kwiver::arrows::core::transfer_bbox_with_depth_map
    :project: kwiver
    :members:

.. _core_transform_detected_object_set:

Transform Detected Object Set Algorithm
----------------------------------------

..  doxygenclass:: kwiver::arrows::core::transform_detected_object_set
    :project: kwiver
    :members:

.. _core_uv_unwrap_mesh:

UV Unwrap Mesh Algorithm
-------------------------

..  doxygenclass:: kwiver::arrows::core::uv_unwrap_mesh
    :project: kwiver
    :members:

.. _core_video_input_buffered_metadata_filter:

Video Input Buffered Metadata Filter Algorithm
-----------------------------------------------

..  doxygenclass:: kwiver::arrows::core::video_input_buffered_metadata_filter
    :project: kwiver
    :members:

.. _core_video_input_filter:

Video Input Filter Algorithm
----------------------------

..  doxygenclass:: kwiver::arrows::core::video_input_filter
    :project: kwiver
    :members:

.. _core_video_input_image_list:

Video Input Image list Algorithm
--------------------------------

..  doxygenclass:: kwiver::arrows::core::video_input_image_list
    :project: kwiver
    :members:

.. _core_video_input_metadata_filter:

Video Input Metadata Filter Algorithm
--------------------------------------

..  doxygenclass:: kwiver::arrows::core::video_input_metadata_filter
    :project: kwiver
    :members:

.. _core_video_input_pos:

Video Input Pos Algorithm
-------------------------

..  doxygenclass:: kwiver::arrows::core::video_input_pos
    :project: kwiver
    :members:

.. _core_video_input_splice:

Video Input Splice Algorithm
-----------------------------

..  doxygenclass:: kwiver::arrows::core::video_input_splice
    :project: kwiver
    :members:

.. _core_video_input_split:

Video Input Split Algorithm
---------------------------

..  doxygenclass:: kwiver::arrows::core::video_input_split
    :project: kwiver
    :members:

.. _core_write_object_track_set_kw18:

Write Object Track Set kw18 Algorithm
--------------------------------------

..  doxygenclass:: kwiver::arrows::core::write_object_track_set_kw18
    :project: kwiver
    :members:


.. _core_write_track_descriptor_set_csv:

Write Track Descriptor Set CSV Algorithm
-----------------------------------------

..  doxygenclass:: kwiver::arrows::core::write_track_descriptor_set_csv
    :project: kwiver
    :members:
