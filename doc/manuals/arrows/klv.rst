KLV
===

.. _klv_apply_child:

Apply Child KLV Algorithm
-------------------------

..  doxygenclass:: kwiver::arrows::klv::_klv_apply_child
    :project: kwiver
    :members:


.. _klv_update:

Update KLV Algorithm
-------------------

..  doxygenclass:: kwiver::arrows::klv::update_klv
    :project: kwiver
    :members:
