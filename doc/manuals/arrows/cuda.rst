CUDA
====

.. _cuda_integrate_depth_maps:

Integrate Depth Maps Algorithm
-------------------------------

..  doxygenclass:: kwiver::arrows::cuda::integrate_depth_maps
    :project: kwiver
    :members:
