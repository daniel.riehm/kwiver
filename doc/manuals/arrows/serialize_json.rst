Serialize JSON
==============

.. _json_activity_type:

Serialize JSON Activity Type Algorithm
---------------------------------------

..  doxygenclass:: kwiver::arrows::serialize::json::activity_type
    :project: kwiver
    :members:


.. _json_activity:

Serialize JSON Activity Algorithm
-----------------------------------

..  doxygenclass:: kwiver::arrows::serialize::json::activity
    :project: kwiver
    :members:


.. _json_bounding_box:

Serialize JSON Bounding Box Algorithm
--------------------------------------

..  doxygenclass:: kwiver::arrows::serialize::json::bounding_box
    :project: kwiver
    :members:


.. _json_detected_object_set:

Serialize JSON Detected Object Set Algorithm
---------------------------------------------

..  doxygenclass:: kwiver::arrows::serialize::json::detected_object_set
    :project: kwiver
    :members:


.. _json_detected_object_type:

Serialize JSON Detected Object Type Algorithm
----------------------------------------------

..  doxygenclass:: kwiver::arrows::serialize::json::detected_object_type
    :project: kwiver
    :members:


.. _json_detected_object:

Serialize JSON Detected Object Algorithm
-----------------------------------------

..  doxygenclass:: kwiver::arrows::serialize::json::detected_object
    :project: kwiver
    :members:


.. _json_image:

Serialize JSON Image Algorithm
-------------------------------

..  doxygenclass:: kwiver::arrows::serialize::json::image
    :project: kwiver
    :members:


.. _json_metadata_map_io:

Serialize JSON Metadata Map Input/Output Algorithm
--------------------------------------------------

..  doxygenclass:: kwiver::arrows::serialize::json::metadata_map_io
    :project: kwiver
    :members:


.. _json_metadata:

Serialize JSON Metadata Algorithm
-----------------------------------

..  doxygenclass:: kwiver::arrows::serialize::json::metadata
    :project: kwiver
    :members:


.. _json_object_track_set:

Serialize JSON Object Track Set Algorithm
------------------------------------------

..  doxygenclass:: kwiver::arrows::serialize::json::object_track_set
    :project: kwiver
    :members:


.. _json_object_track_state:

Serialize JSON Object Track State Algorithm
--------------------------------------------

..  doxygenclass:: kwiver::arrows::serialize::json::object_track_state
    :project: kwiver
    :members:


.. _json_string:

Serialize JSON String Algorithm
---------------------------------

..  doxygenclass:: kwiver::arrows::serialize::json::string
    :project: kwiver
    :members:


.. _json_timestamp:

Serialize JSON Timestamp Algorithm
------------------------------------

..  doxygenclass:: kwiver::arrows::serialize::json::timestamp
    :project: kwiver
    :members:


.. _json_track_set:

Serialize JSON Track Set Algorithm
-----------------------------------

..  doxygenclass:: kwiver::arrows::serialize::json::track_set
    :project: kwiver
    :members:


.. _json_track_state:

Serialize JSON Track State Algorithm
--------------------------------------

..  doxygenclass:: kwiver::arrows::serialize::json::track_state
    :project: kwiver
    :members:


.. _json_track:

Serialize JSON Track Algorithm
-------------------------------

..  doxygenclass:: kwiver::arrows::serialize::json::track
    :project: kwiver
    :members:
